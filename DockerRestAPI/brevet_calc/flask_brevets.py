"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import redirect, url_for, request
from flask_restful import Resource, Api # For building API
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import logging
import sys
import os
from pymongo import MongoClient

###
# Globals
###
app = flask.Flask(__name__)
api = Api(app)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

#client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
client = MongoClient('mongodb://mongodb:27017/')

db = client.tododb

###
# Resources
###

class ALL_Time(Resource):
    def get(self):
        return {
            'open': [db.tododb.find({"name":"open_time"})],
            'close':[db.tododb.find({"name":"close_time"})]
        }
class Open_Time(Resource):
    def get(self):
        return {
            'open': [db.tododb.find({"name":"open_time"})]
        }
class Close_Time(Resource):
    def get(self):
        return {
            'close': [db.tododb.find({"name":"close_time"})]
        }


api.add_resource(ALL_Time, '/listAll')
api.add_resource(Open_Time, '/listOpenOnly')
api.add_resource(Close_Time, '/listCloseOnly')



###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")

    km = request.args.get('km', 999, type=float)
    tdistance = request.args.get('tdistance', type=str)
    begin_date = request.args.get('begin_date', type=str)
    begin_time = request.args.get('begin_time', type=str)

    start_date = begin_date + " " + begin_time
    date = arrow.get(start_date, "YYYY-MM-DD HH:mm").isoformat()

    open_time = acp_times.open_time(km, tdistance, date)
    close_time = acp_times.close_time(km, tdistance, date)

    result = {"open": open_time, "close": close_time}


    return flask.jsonify(result=result)

# Function for submiting data
@app.route('/new', methods=["POST"])
def new():
    open_t = {
        'name': 'open_time',
        'description': request.form.get("open")
        }
    db.tododb.insert_one(open_t)

    close_t = {
        'name': 'close_time',
        'description':request.form.get("close")
        }
    db.tododb.insert_one(close_t)

    return redirect(url_for('index'))

# This function is suppose to render display.html with database info
@app.route("/display", methods=["POST"])
def display():
    _oitems = db.tododb.find({"name":"open_time"})
    oitems = [item for item in _oitems]

    if len(oitems) == 0:
        return flask.render_template("empty.html")

    _citems = db.tododb.find({"name":"close_time"})
    citems = [item for item in _citems]

    return flask.render_template('display.html', oitems=oitems, citems=citems)




#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
